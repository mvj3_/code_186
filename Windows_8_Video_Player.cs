<AppBar Margin="382,0,382,688" Background="Black"> 
            <StackPanel Margin="0,0,0,0" Orientation="Horizontal"> 
                <Button Style="{StaticResource MyDefinedStyleButton}" Click="Pause_Click" HorizontalAlignment="Left" VerticalAlignment="Top" ToolTipService.ToolTip="Pause media." > 
                    <Image Source="Icons/pause.png" /> 
                </Button> 
                <Button Style="{StaticResource MyDefinedStyleButton}" Click="Play_Click" HorizontalAlignment="Left" VerticalAlignment="Top" ToolTipService.ToolTip="Play media" > 
                    <Image Source="Icons/play.png" /> 
                </Button> 
                <Button Content="Open File" HorizontalAlignment="Left" Margin="0,0,0,20" VerticalAlignment="Top" Height="60" Width="132" Click="OpenFile_Click" ToolTipService.ToolTip="Choose a file to play."/> 
                <StackPanel Orientation="Vertical" Width="326"> 
                    <Slider x:Name="sliderVolume" Width="259" ValueChanged="VulumeChangedEvent" BorderThickness="2" ToolTipService.ToolTip="Volume" Orientation="Horizontal" RenderTransformOrigin="0.5,0.5" Height="39" /> 
                    <StackPanel Orientation="Horizontal" Height="36">                        
                        <TextBlock Name="MediaCurrentPosition" Width="110" Margin="50,0,0,0" ToolTipService.ToolTip="The current displaying position."/> 
                        <TextBlock Name="MediaDuration" Width="118" ToolTipService.ToolTip="The duration of this media" /> 
                    </StackPanel> 
                </StackPanel> 
            </StackPanel> 
        </AppBar>